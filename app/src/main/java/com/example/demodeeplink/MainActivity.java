package com.example.demodeeplink;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private static final String PARAM_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        Uri uri = intent.getData();

        if (uri != null) {
            String id = uri.getQueryParameter(PARAM_ID);
            startActivity(new Intent(this, ResultActivity.class).putExtra("ID", id));

        } else {
            Toast.makeText(this, "Chưa nhận được dữ liệu", Toast.LENGTH_LONG).show();
        }
    }
}
